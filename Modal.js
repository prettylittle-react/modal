import React from 'react';
import PropTypes from 'prop-types';

//import Text from 'text';
//import Heading from 'heading';
import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import EventEmitter from 'eventemitter';

import {default as ReactModal} from 'react-modal';

import {UiClose} from 'ui';

import './Modal.scss';

ReactModal.defaultStyles = {
	overlay: {},
	content: {}
};

/**
 * Modal
 * @description [description]
 * @example
  <div id="Modal"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Modal, {
	}), document.getElementById("Modal"));
  </script>
 */
class Modal extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'popup';
		this.modalClass = 'modal';

		this.state = {
			isOpen: props.isOpen
		};

		this.modalElement = null;
		this.modalContent = null;

		this.onRequestClose = this.close.bind(this);
		this.onRequestOpen = this.open.bind(this);
	}

	onHashChange = ev => {
		this.hashCheck(ev.target.location.hash);
	};

	hashCheck(hash, anchor = false) {
		const {id} = this.props;

		if (!id) {
			return null;
		}

		if (hash) {
			const windowId = hash.substr(1);

			if (windowId === id) {
				this.open();
			}
		}
	}

	componentDidMount() {
		const {id} = this.props;

		if (id) {
			EventEmitter.subscribe(`modal[${id}]-close`, this.onRequestClose);
		}

		EventEmitter.subscribe(`modal-close-all`, this.onRequestClose);

		const {useHash} = this.props;

		this.hashCheck(window.location.hash, true);

		if (useHash) {
			window.addEventListener('hashchange', this.onHashChange, false);
		}
	}

	conponentWillUnmount() {
		const {useHash} = this.props;
		if (useHash) {
			window.removeEventListener('hashchange', this.onHashChange);
		}
	}

	onModalClick = e => {
		if (e.target === this.modal) {
			this.close();
		}
	};

	onAfterOpen = () => {
		const {onAfterOpen} = this.props;

		if (onAfterOpen) {
			onAfterOpen.call();
		}
	};

	componentWillReceiveProps(nextProps) {
		const {isOpen} = this.state;

		if (nextProps.isOpen && isOpen === false) {
			this.open();
		} else if (nextProps.isOpen === false && isOpen) {
			this.close();
		} else {
			this.setState(nextProps);
		}
	}

	close() {
		if (!SERVER) {
			// reset body width
			setTimeout(()=>{
				document.body.style.width = '100%';
			}, 350);
		}

		// document.body.style.width = '100%';
		const {useHash} = this.props;
		const {isOpen} = this.state;

		if (isOpen === false) {
			return;
		}

		this.setState(
			{
				isOpen: false
			},
			() => {
				const {onClose, id, useHash} = this.props;

				EventEmitter.dispatch(`modal[${id}]-closed`);

				if (onClose) {
					onClose.call();
				}

				// TODO: check if the window id is still the same as the modal id before changing this
				if (useHash) {
					window.location.hash = '';
				}
			}
		);
	}

	open() {
		if (!SERVER) {
			// set body width to prevent it jumping when the scroll bar disappears
			const body = document.body;
			const scrollWidth = window.innerWidth - body.offsetWidth;
			body.style.width = `calc(100% - ${scrollWidth}px)`;
		}

		const {isOpen} = this.state;

		if (isOpen === true) {
			return;
		}

		const {doCloseOthersOnOpen} = this.props;

		if (doCloseOthersOnOpen) {
			EventEmitter.dispatch(`modal-close-all`);
		}

		this.setState({isOpen: true}, () => {});
	}

	getClose() {
		const {closeText, isDismissable} = this.props;

		if (!isDismissable) {
			return null;
		}

		return <UiClose className={'modal__close'} onClick={this.onRequestClose} title={closeText} />;
	}

	getInner() {
		const {hasChrome, headerAfter} = this.props;

		const header = this.getTitle();
		const head =
			typeof header === 'string' ? (
				<Heading h={3} content={header} className={`${this.modalClass}__title`} />
			) : (
				header
			);

		if (hasChrome) {
			return (
				<div className={`${this.modalClass}__chrome`}>
					<div className={`${this.modalClass}__head`}>
						<div className={`${this.modalClass}__head-inner`}>
							<div className={`${this.modalClass}__head-title`}>{head}</div>
							{this.getClose()}
							{headerAfter}
						</div>
					</div>
					<div className={`${this.modalClass}__body`}>
						<div ref={container => (this.container = container)}>{this.getContent()}</div>
					</div>
				</div>
			);
		}

		return (
			<div>
				<div ref={container => (this.container = container)}>{this.getContent()}</div>
				{this.getClose()}
			</div>
		);
	}

	getContent() {
		const {children} = this.props;

		return children;
	}

	getTitle() {
		const {title, header} = this.props;

		if (header === '') {
			return title;
		}

		return header;
	}

	render() {
		const {
			isDismissable,
			isDynamic,
			openTimeout,
			closeTimeout,
			position,
			hasChrome,
			size,
			openText,
			openClass,
			title
		} = this.props;

		const {isOpen} = this.state;

		const overlayClass = {
			base: `${this.modalClass} ${this.modalClass}--${hasChrome ? 'chrome' : 'chromeless'} ${
				this.modalClass
			}--${position} ${this.modalClass}--${size}`,
			afterOpen: `active ${this.modalClass}--after-open`,
			beforeClose: `${this.modalClass}--before-close`
		};

		const wrapperClass = {
			base: `${this.modalClass}__content`,
			afterOpen: `${this.modalClass}__content-after-open`,
			beforeClose: `${this.modalClass}__content-before-close`
		};

		return (
			<div className={`${this.baseClass}`}>
				{openText && (
					<button className={`${this.modalClass}__open ${openClass}`} onClick={this.onRequestOpen}>
						<Text html={openText} />
					</button>
				)}
				{isDynamic ? (
					<ReactModal
						isOpen={isOpen}
						onAfterOpen={this.onAfterOpen}
						onRequestClose={isDismissable ? this.onRequestClose : null}
						contentLabel={title}
						openTimeoutMS={openTimeout}
						closeTimeoutMS={closeTimeout}
						className={wrapperClass}
						overlayClassName={overlayClass}
						bodyOpenClassName={`${this.modalClass}-open`}
						ariaHideApp={false}
					>
						{this.getInner()}
					</ReactModal>
				) : (
					<div
						className={`${overlayClass.base} ${this.modalClass}--placeholder ${
							isOpen ? overlayClass.afterOpen : ''
						}`}
						onClick={this.onModalClick}
						ref={modal => (this.modal = modal)}
					>
						<div className={wrapperClass.base}>{this.getInner()}</div>
					</div>
				)}
			</div>
		);
	}
}

Modal.defaultProps = {
	useHash: false,
	isDismissable: true,
	doCloseOthersOnOpen: false,
	isOpen: false,
	id: '',
	isDynamic: true,
	closeText: 'Close',
	openText: '',
	openClass: '',
	size: 'default',
	position: 'right',
	title: '',
	header: '',
	hasChrome: true,
	children: '',
	closeTimeout: 650,
	openTimeout: 650,
	onClose: null,
	onAfterOpen: null
};

Modal.propTypes = {
	doCloseOthersOnOpen: PropTypes.bool,
	useHash: PropTypes.bool,
	isDismissable: PropTypes.bool,
	isOpen: PropTypes.bool,
	children: PropTypes.any,
	closeText: PropTypes.string,
	closeTimeout: PropTypes.number,
	hasChrome: PropTypes.bool,
	header: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
	id: PropTypes.string,
	isDynamic: PropTypes.bool,
	openClass: PropTypes.string,
	openText: PropTypes.string,
	openTimeout: PropTypes.number,
	position: PropTypes.string,
	size: PropTypes.string,
	title: PropTypes.string,
	onAfterOpen: PropTypes.func,
	onClose: PropTypes.func
};

export default Modal;
